package com.example.uapv1900315.tp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class CityActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;
    private SwipeRefreshLayout swipeRefreshLayout;
    WeatherDbHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        Intent in= getIntent();
        db=new WeatherDbHelper(this);
        Bundle extras = getIntent().getExtras();
        ///String citynama=extras.getString("varkey");
        city = (City)extras.get("mycity");

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        swipeRefreshLayout = findViewById(R.id.refresh_layout_city);
        swipeRefreshLayout.setOnRefreshListener(this);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MainActivity.testIfIsConnected(CityActivity.this)) {
                    swipeRefreshLayout.setRefreshing(true);
                    new UpdateAsyncTask().execute();

                }else{
                    swipeRefreshLayout.setRefreshing(false);
                    new AlertDialog.Builder(CityActivity.this)
                            .setTitle("Not Connected")
                            .setMessage("You are not connected")
                            .create().show();
                }
            }
        });

    }

    @Override
    public void onRefresh() {
        if (MainActivity.testIfIsConnected(CityActivity.this)) {
            new UpdateAsyncTask().execute();
        }else{
            swipeRefreshLayout.setRefreshing(false);
            new AlertDialog.Builder(CityActivity.this)
                    .setTitle("Not Connected")
                    .setMessage("You are not connected")
                    .create().show();
        }
    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity()+" %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.cityS, city);
        setResult(Activity.RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }


    class UpdateAsyncTask extends AsyncTask<Object, Integer, City> {
        @Override
        protected City doInBackground(Object... objects) {

            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return city;
        }

        @Override
        protected void onPostExecute(City city) {
            updateView();
            db.updateCity(city);
            swipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(city);

        }
    }



}
