package com.example.uapv1900315.tp3;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    City citye;
    TextView cName = null;
    TextView cCountry = null;
    TextView temperature = null;
    ImageView imageViewRow = null;
    ConstraintLayout c;

    public RowHolder(View row) {
        super(row);
        cName = (TextView)row.findViewById(R.id.cName);
        cCountry = (TextView)row.findViewById(R.id.cCountry);
        temperature = (TextView)row.findViewById(R.id.temperature);
        imageViewRow = (ImageView)row.findViewById(R.id.imageViewRow);
        c = (ConstraintLayout)row.findViewById(R.id.rowrecy);
    }


    @Override
    public void onClick(View v) {

    }

    void bindModel(City city){

    }
}
