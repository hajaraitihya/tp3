package com.example.uapv1900315.tp3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    WeatherDbHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);
        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);
        final Button but = (Button) findViewById(R.id.button);
        db=new WeatherDbHelper(this);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                City city = new City(textName.getText().toString(), textCountry.getText().toString());
                db.addCity(city);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

}
