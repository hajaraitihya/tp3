package com.example.uapv1900315.tp3;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<City> cities;
    WeatherDbHelper db;
    MyRecyAdapter mAdapter;
    RecyclerView recy;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static final String cityS = "city";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        swipeRefreshLayout = findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                    if (MainActivity.testIfIsConnected(MainActivity.this)) {
                        new WheatherTask().execute();

                    }else{
                        swipeRefreshLayout.setRefreshing(false);
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Not Connected")
                                .setMessage("You are not connected")
                                .create().show();
                    }

                }

        });
        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, 2);

            }
        });
        recy = (RecyclerView) findViewById(R.id.recy) ;
        db=new WeatherDbHelper(this);
       db.populate();
        cities= new ArrayList<City>();
        cities=db.getAllCities();
        recy.setLayoutManager(new LinearLayoutManager(this));
        recy.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter= new MyRecyAdapter(cities);
        myUpItemTouchHelper();
        recy.setAdapter(mAdapter);


    }
    private void myUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int pos = viewHolder.getAdapterPosition();
                City city=cities.get(pos);
                int id =(int) city.getId();
                db.deleteCity(id);
                mAdapter.remove(pos);
            }

        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recy);
    }
    public static boolean testIfIsConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netWorkInfo = connectivityManager.getActiveNetworkInfo();
        return netWorkInfo != null && netWorkInfo.isConnectedOrConnecting();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (2 == requestCode && RESULT_OK == resultCode) {
            cities=db.getAllCities();
            mAdapter.citiess=cities;
            mAdapter.notifyDataSetChanged();
            //recy.setAdapter(new MyRecyAdapter(cities));
            recy.setAdapter(mAdapter);

        }
        if (1 == requestCode && RESULT_OK == resultCode) {
            cities=db.getAllCities();
            mAdapter.citiess=cities;
            mAdapter.notifyDataSetChanged();
            //recy.setAdapter(new MyRecyAdapter(cities));
            recy.setAdapter(mAdapter);
        }

    }

    class MyRecyAdapter extends RecyclerView.Adapter<RowHolder>{
        private List<City> citiess;
        City city;
        public MyRecyAdapter(List<City> citie){
            citiess=citie;
        }
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row,parent,false)));
        }

        @Override
        public void onBindViewHolder(@NonNull final RowHolder holder, int position) {
           //citiess=db.getAllCities();
            city=citiess.get(position);
            holder.bindModel(cities.get(position));
            holder.cName.setText(city.getName());
            holder.cCountry.setText(city.getCountry());

            holder.temperature.setText(city.getTemperature()+" °C");
            holder.citye=city;
            holder.c.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), CityActivity.class);
                    Bundle bundle = new Bundle();
                    intent.putExtra("mycity",holder.citye);
                    ((Activity)v.getContext()).startActivityForResult(intent,1);
                }
            });
            if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
                holder.imageViewRow.setImageDrawable(getResources().getDrawable(getResources()
                        .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
                holder.imageViewRow.setContentDescription(city.getDescription());
            }

        }

        @Override
        public int getItemCount() {
            return (cities.size());
        }
        public void remove(int position) {
            if (position < 0 || position >= citiess.size()) {
                return;
            }
            citiess.remove(position);
            cities = db.getAllCities();
            mAdapter.citiess=cities;
            recy.setAdapter(mAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class WheatherTask extends AsyncTask<Object, Integer, List<City>> {
        private List<City> cityList;

        @Override
        protected void onPreExecute() {
            cityList = db.getAllCities();
            mAdapter.citiess=cityList;
            mAdapter.notifyDataSetChanged();
            recy.setAdapter(mAdapter);

            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            HttpURLConnection urlConnection = null;
            cityList = db.getAllCities();
            for (City city : cityList) {

                try {
                    // prepare url
                    URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                    // send a GET request to the serve
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();
                    // read data
                    InputStream inputStream = urlConnection.getInputStream();
                    JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                    jsonHandler.readJsonStream(inputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
                db.updateCity(city);
            }
            return cityList;
        }


        @Override
        protected void onPostExecute(List<City> cities) {

            cityList = db.getAllCities();
            mAdapter.citiess=cityList;
            recy.setAdapter(mAdapter);
            swipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(cities);


        }


    }
}
